#include "Tests.h"

//Test functions
//Null operation, or baseline
uint16_t TEST0 (void) {
	uint16_t start;
	int i;
	unsigned total;
	
	total = 0;
	for (i = 0; i < TEST_ITERATIONS; i += 1) {
		start = TimerStart();
		total += TimerStop(start);
	}
	
	//Return average
	return total / TEST_ITERATIONS;
}

//Add 2 random 32-bit integers
uint16_t TEST1 (void) {
	uint16_t start;
	int i;
	int32_t r1, r2;
	volatile int32_t r3;
	unsigned total;
	
	total = 0;
	for (i = 0; i < TEST_ITERATIONS; i += 1) {
		r1 = rand();
		r2 = rand();
		start = TimerStart();
		r3 = r1 + r2;
		total += TimerStop(start);
	}
	
	//Return average
	return total / TEST_ITERATIONS;
}

//Add 2 random 64-bit integers
uint16_t TEST2 (void) {
	uint16_t start;
	int i;
	int64_t r1, r2;
	volatile int64_t r3;
	unsigned total;
	
	total = 0;
	for (i = 0; i < TEST_ITERATIONS; i += 1) {
		r1 = rand() + (rand() << 31);
		r2 = rand() + (rand() << 31);
		start = TimerStart();
		r3 = r1 + r2;
		total += TimerStop(start);
	}
	
	//Return average
	return total / TEST_ITERATIONS;
}

//Multiply two random 32-bit integers
uint16_t TEST3 (void) {
	uint16_t start;
	int i;
	int32_t r1, r2;
	volatile int32_t r3;
	unsigned total;
	
	total = 0;
	for (i = 0; i < TEST_ITERATIONS; i += 1) {
		r1 = rand();
		r2 = rand();
		start = TimerStart();
		r3 = r1 * r2;
		total += TimerStop(start);
	}
	
	//Return average
	return total / TEST_ITERATIONS;
}

//Multiply two random 32-bit integers
uint16_t TEST4 (void) {
	uint16_t start;
	int i;
	int64_t r1, r2;
	volatile int64_t r3;
	unsigned total;
	
	total = 0;
	for (i = 0; i < TEST_ITERATIONS; i += 1) {
		r1 = rand() + (rand() << 31);
		r2 = rand() + (rand() << 31);
		start = TimerStart();
		r3 = r1 * r2;
		total += TimerStop(start);
	}
	
	//Return average
	return total / TEST_ITERATIONS;
}

//Divide two random 32-bit integers
uint16_t TEST5 (void) {
	uint16_t start;
	int i;
	int32_t r1, r2;
	volatile int32_t r3;
	unsigned total;
	
	total = 0;
	for (i = 0; i < TEST_ITERATIONS; i += 1) {
		r1 = rand();
		r2 = rand();
		start = TimerStart();
		if(r2 != 0)
			r3 = r1 / r2;
		total += TimerStop(start);
	}
	
	//Return average
	return total / TEST_ITERATIONS;
}

//Divide 2 random 64-bit integers
uint16_t TEST6 (void) {
	uint16_t start;
	int i;
	int64_t r1, r2;
	volatile int64_t r3;
	unsigned total;
	
	total = 0;
	for (i = 0; i < TEST_ITERATIONS; i += 1) {
		r1 = rand() + (rand() << 31);
		r2 = rand() + (rand() << 31);
		start = TimerStart();
		if (r2 != 0)
			r3 = r1 / r2;
		total += TimerStop(start);
	}
	
	//Return average
	return total / TEST_ITERATIONS;
}

//Copy an 8-byte struct using the assignment operator
uint16_t TEST7 (void) {
	uint16_t start;
	int i;
	struct testStruct8 s1;
	volatile struct testStruct8 s2;
	unsigned total;
	
	total = 0;
	for (i = 0; i < TEST_ITERATIONS; i += 1) {
		start = TimerStart();
		s2 = s1;
		total += TimerStop(start);
	}
	
	//Return average
	return total / TEST_ITERATIONS;
}

//Copy a 128-byte struct using the assignment operator
uint16_t TEST8 (void) {
	uint16_t start;
	int i;
	struct testStruct128 s1;
	volatile struct testStruct128 s2;
	unsigned total;
	
	total = 0;
	for (i = 0; i < TEST_ITERATIONS; i += 1) {
		start = TimerStart();
		s2 = s1;
		total += TimerStop(start);
	}
	
	//Return average
	return total / TEST_ITERATIONS;
}

//Copy a 1024-byte struct using the assignment operator
uint16_t TEST9 (void) {
	uint16_t start;
	int i;
	volatile struct testStruct1024 s1;
	unsigned total;
	
	total = 0;
	for (i = 0; i < TEST_ITERATIONS; i += 1) {
		start = TimerStart();
		s1 = s1;//Assign it to itself to avoid stack overflow with certain optimization levels
		total += TimerStop(start);
	}
	
	//Return average
	return total / TEST_ITERATIONS;
}
