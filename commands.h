#ifndef COMMANDS_H
#define COMMANDS_H

#include <stdint.h>
#include <cstdlib>
#include "registers.h"
#include "UART.h"
#include "Timer.h"
#include "Tests.h"

//Limit the command length and define length of command list
#define COMMAND_LENGTH 20
#define COMMAND_LIST_LENGTH 6

//These stings are compared against the imput string to find a matching function call
static const char COMMANDLIST[COMMAND_LIST_LENGTH][COMMAND_LENGTH] = {
	"LED ON ",
	"LED OFF ",
	"HELP",
	"QUERY LED ",
	"INFO",
	"TEST "
};

void LED_ON (char * LED);
void LED_OFF (char * LED);
void HELP (char * data);
void QUERY_LED (char * LED);
void INFO (char * data);
void ERROR (char * data);
void TEST (char * data);

//This list of functions corresponds to the above list of match strings
static void (*functionCalls[COMMAND_LIST_LENGTH])(char * data) = {
	LED_ON,
	LED_OFF,
	HELP,
	QUERY_LED,
	INFO,
	TEST
};
#endif
