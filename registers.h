/*
* This file defines a selection of useful registers such as
* clock registers and GPIO registers. It is likely that this
* file will be expanded as time goes on. Currently, the
* registers are all treated as pointers and need to be
* dereferenced accordingly in the main program when used.
*/

#ifndef REGISTERS_H
#define REGISTERS_H
#include <stdint.h>

#define PERIPH_BASE           ((uint32_t)0x40000000)
#define AHBPERIPH_BASE        (PERIPH_BASE + 0x20000)
#define RCC_BASE              (AHBPERIPH_BASE + 0x1000)
#define RCC_APB1ENR           (* (uint32_t volatile *)(RCC_BASE + 0x1C))
#define RCC_APB2ENR           (* (uint32_t volatile *)(RCC_BASE + 0x18))
#define APB2PERIPH_BASE       (PERIPH_BASE + 0x10000)

//GPIOB holds the LEDs on pins 8 through 15
#define GPIOB_BASE            (APB2PERIPH_BASE + 0x0C00)
#define GPIOB_ODR             (* (uint32_t volatile *)(GPIOB_BASE + 0x0C))
#define GPIOB_CRL             (* (uint32_t volatile *)(GPIOB_BASE + 0x00))
#define GPIOB_CRH             (* (uint32_t volatile *)(GPIOB_BASE + 0x04))
#define GPIOB_BSRR            (* (uint32_t volatile *)(GPIOB_BASE + 0x10))
#define GPIOB_BRR             (* (uint32_t volatile *)(GPIOB_BASE + 0x14))
#define GPIOB_IDR             (* (uint32_t volatile *)(GPIOB_BASE + 0x08))

//GPIOA holds the UART TX/RX pins on pins 2 and 3, respectively
#define GPIOA_BASE            (APB2PERIPH_BASE + 0x0800)
#define GPIOA_ODR             (* (uint32_t volatile *)(GPIOA_BASE + 0x0C))
#define GPIOA_CRL             (* (uint32_t volatile *)(GPIOA_BASE + 0x00))
#define GPIOA_CRH             (* (uint32_t volatile *)(GPIOA_BASE + 0x04))
#define GPIOA_BSRR            (* (uint32_t volatile *)(GPIOA_BASE  + 0x10))
#define GPIOA_BRR             (* (uint32_t volatile *)(GPIOA_BASE  + 0x14))
#define GPIOA_IDR             (* (uint32_t volatile *)(GPIOA_BASE + 0x08))

//USART 2 stuff
#define USART2_BASE           (PERIPH_BASE + 0x4400)
#define USART2_SR             (* (uint32_t volatile *)(USART2_BASE + 0x00))
#define USART2_DR             (* (uint32_t volatile *)(USART2_BASE + 0x04))
#define USART2_BRR            (* (uint32_t volatile *)(USART2_BASE + 0x08))
#define USART2_CR1            (* (uint32_t volatile *)(USART2_BASE + 0x0C))
#define USART2_CR2            (* (uint32_t volatile *)(USART2_BASE + 0x10))
#define USART2_CR3            (* (uint32_t volatile *)(USART2_BASE + 0x14))
#define USART2_GTPR           (* (uint32_t volatile *)(USART2_BASE + 0x18))

//TIM2 stuff
#define TIM2_BASE             (PERIPH_BASE + 0x0000)
#define TIM2_CR1              (* (uint32_t volatile *)(TIM2_BASE + 0x00))
#define TIM2_CR2              (* (uint32_t volatile *)(TIM2_BASE + 0x04))
#define TIM2_SMCR             (* (uint32_t volatile *)(TIM2_BASE + 0x08))
#define TIM2_DIER             (* (uint32_t volatile *)(TIM2_BASE + 0x0C))
#define TIM2_SR               (* (uint32_t volatile *)(TIM2_BASE + 0x10))
#define TIM2_EGR              (* (uint32_t volatile *)(TIM2_BASE + 0x14))
#define TIM2_CCMR1            (* (uint32_t volatile *)(TIM2_BASE + 0x18))
#define TIM2_CCMR2            (* (uint32_t volatile *)(TIM2_BASE + 0x1C))
#define TIM2_CCER             (* (uint32_t volatile *)(TIM2_BASE + 0x20))
#define TIM2_CNT              (* (uint32_t volatile *)(TIM2_BASE + 0x24))
#define TIM2_PSC              (* (uint32_t volatile *)(TIM2_BASE + 0x28))
#define TIM2_ARR              (* (uint32_t volatile *)(TIM2_BASE + 0x2C))
#define TIM2_CCR1             (* (uint32_t volatile *)(TIM2_BASE + 0x34))
#define TIM2_CCR2             (* (uint32_t volatile *)(TIM2_BASE + 0x38))
#define TIM2_CCR3             (* (uint32_t volatile *)(TIM2_BASE + 0x3C))
#define TIM2_CCR4             (* (uint32_t volatile *)(TIM2_BASE + 0x40))
#define TIM2_DCR              (* (uint32_t volatile *)(TIM2_BASE + 0x48))
#define TIM2_DMAR             (* (uint32_t volatile *)(TIM2_BASE + 0x4C

//TIM3 stuff
#define TIM3_BASE             (PERIPH_BASE + 0x0400)
#define TIM3_CR1              (* (uint32_t volatile *)(TIM3_BASE + 0x00))
#define TIM3_CR2              (* (uint32_t volatile *)(TIM3_BASE + 0x04))
#define TIM3_SMCR             (* (uint32_t volatile *)(TIM3_BASE + 0x08))
#define TIM3_DIER             (* (uint32_t volatile *)(TIM3_BASE + 0x0C))
#define TIM3_SR               (* (uint32_t volatile *)(TIM3_BASE + 0x10))
#define TIM3_EGR              (* (uint32_t volatile *)(TIM3_BASE + 0x14))
#define TIM3_CCMR1            (* (uint32_t volatile *)(TIM3_BASE + 0x18))
#define TIM3_CCMR2            (* (uint32_t volatile *)(TIM3_BASE + 0x1C))
#define TIM3_CCER             (* (uint32_t volatile *)(TIM3_BASE + 0x20))
#define TIM3_CNT              (* (uint32_t volatile *)(TIM3_BASE + 0x24))
#define TIM3_PSC              (* (uint32_t volatile *)(TIM3_BASE + 0x28))
#define TIM3_ARR              (* (uint32_t volatile *)(TIM3_BASE + 0x2C))
#define TIM3_CCR1             (* (uint32_t volatile *)(TIM3_BASE + 0x34))
#define TIM3_CCR2             (* (uint32_t volatile *)(TIM3_BASE + 0x38))
#define TIM3_CCR3             (* (uint32_t volatile *)(TIM3_BASE + 0x3C))
#define TIM3_CCR4             (* (uint32_t volatile *)(TIM3_BASE + 0x40))
#define TIM3_DCR              (* (uint32_t volatile *)(TIM3_BASE + 0x48))
#define TIM3_DMAR             (* (uint32_t volatile *)(TIM3_BASE + 0x4C))

//NVIC1
#define NVIC_BASE             0xE000E100
#define NVIC_SET_EN           (* (uint32_t volatile *)(NVIC_BASE + 0x00))
#define NVIC_CLEAR_EN         (* (uint32_t volatile *)(NVIC_BASE + 0x80))
#define NVIC_SET_PEND         (* (uint32_t volatile *)(NVIC_BASE + 0x100))
#define NVIC_CLEAR_PEND       (* (uint32_t volatile *)(NVIC_BASE + 0x180))
#define NVIC_ABR              (* (uint32_t volatile *)(NVIC_BASE + 0x200))
#define NVIC_PR               (* (uint32_t volatile *)(NVIC_BASE + 0x300))

//Interrupt stuff
#define EXTI_BASE             (PERIPH_BASE + 0x10400)
#define EXTI_IMR              (* (uint32_t volatile *)(EXTI_BASE + 0x00))
#define EXTI_EMR              (* (uint32_t volatile *)(EXTI_BASE + 0x04))
#define EXTI_RTSR             (* (uint32_t volatile *)(EXTI_BASE + 0x08))
#define EXTI_FTSR             (* (uint32_t volatile *)(EXTI_BASE + 0x0C))
#define EXTI_SWIER            (* (uint32_t volatile *)(EXTI_BASE + 0x10))
#define EXTI_PR               (* (uint32_t volatile *)(EXTI_BASE + 0x14))

#endif      
