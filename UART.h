#ifndef UART_H
#define UART_H
#include "registers.h"

//Initialize the UART peripherals
int Serial_Open(void);
//Deactive all UART features
int Serial_Close(void);
//Begin communications and enable the device
uint8_t GetByte(void);
//Disable the device and stop communications
int SendByte(uint8_t B);
int SendLine(char * str);
int SendInt(int num);

#endif
