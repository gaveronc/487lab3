#include "timer.h"

void InitTimer(void) {
	TIM2_ARR = 0;//Start from 0
	RCC_APB1ENR |= 1;//Enable clock to TIM2
	TIM2_CR1 |= 1 << 4;//Configure as count down (for some reason)
	TIM2_CR1 |= 1;//Enable timer
}

uint16_t TimerStart(void) {
	return TIM2_CNT;//Return count
}

uint16_t TimerStop(int16_t start_time) {
	int time = start_time - TIM2_CNT;
	
	if (time < 0) {//Handle wraparound
		time += 0xFFFF + 1;
	}
	
	return (uint16_t) time;//Return count
}

void TimerShutdown(void) {
	RCC_APB1ENR &= ~1;//Disable clock to TIM2
	TIM2_CR1 &= ~1;//Disable timer
}

void InitTimer1Hz(void) {
	RCC_APB1ENR |= 1 << 1;//Enable clock to TIM3
	TIM3_PSC = 720000;		//Divide clock down to 1KHz
	TIM3_ARR = 1000;			//Trigger interrupt on 1KHz
	TIM3_DIER |= 1;
	TIM3_CR1 |= 1;  			//Enable timer
	
	NVIC_SET_EN |= 1 << 29;//Enable TIM3 interrupt
}
