#ifndef TESTS_H
#define TESTS_H

#define TEST_NUM 10
#define TEST_ITERATIONS 100

#include <stdint.h>
#include <cstdlib>
#include "Timer.h"
#include "UART.h"

//8-byte test struct
static struct testStruct8 {
	char a[8];
} testStruct8;

//128-byte test struct
static struct testStruct128 {
	char a[128];
} testStruct128;

//1024-byte test struct
static struct testStruct1024 {
	char a[1024];
} testStruct1024;


//Test functions called by TEST
uint16_t TEST0 (void);
uint16_t TEST1 (void);
uint16_t TEST2 (void);
uint16_t TEST3 (void);
uint16_t TEST4 (void);
uint16_t TEST5 (void);
uint16_t TEST6 (void);
uint16_t TEST7 (void);
uint16_t TEST8 (void);
uint16_t TEST9 (void);

//Index the test functions
static uint16_t (*TestCalls[TEST_NUM]) (void) = {
	TEST0,
	TEST1,
	TEST2,
	TEST3,
	TEST4,
	TEST5,
	TEST6,
	TEST7,
	TEST8,
	TEST9
};

#endif
